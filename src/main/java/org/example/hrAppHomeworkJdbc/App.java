package org.example.hrAppHomeworkJdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.example.hrAppHomeworkJdbc.model.Employee;

/**
 * 1.Create Maven project. 2.Enter in browser and search mysql connector in
 * Maven Repository. 3.Copy the dependency from the site. 4.Enter in pom.xml and
 * paste the dependency there. 5.Enter the App class. 6.Create the constants
 * DB_URL,USER AND PASSWORD and instance variable connection. 7.Create the
 * connection using DriverManager. 8.Create the methods that hold the queries
 * and do the operations on the table.
 * 
 *
 */
public class App {
	private static final String DB_URL = "jdbc:mysql://localhost:3306/hr?autoReconnect=true&useSSL=false";
	private static final String USER = "root";
	private static final String PASSWORD = "decebalus";
	private static Connection connection;

	public static void main(String[] args) {
		try {
			connection = DriverManager.getConnection(DB_URL, USER, PASSWORD);

			System.out.println("----------0-----------"); // ok
			getEmployeesByRegionName("Europe");
			System.out.println("----------1-----------"); // ok
			getEmployeesInUsaBasedOnSalary();
			System.out.println("---------2-----------"); // x salary
			getMaxSalaryForJobTitle("Programmer");
			System.out.println("----------3-----------"); // ok
			getEmployeesFromCountryName("United States of America");
			System.out.println("----------4-----------"); // ok
			getCountriesFromLocation();
			System.out.println("----------5-----------"); // ok
			getJobForEmployee("Adam", "Fripp");
			System.out.println("----------6-----------"); // ok
			getMinSalaryForEmployee("Geo", "Trif");
			System.out.println("----------7-----------"); // ok
			getDepartmentNameWithHighestNumbersOfEmployees();
			System.out.println("----------8-----------"); // ok
			getCountryNameWithMaxDepartments();
			System.out.println("----------9-----------"); // x salary
			getAverageSalaryForDepartments();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static void getEmployeesByRegionName(String regionName) { // SELECT ALL EMPLOYEES THAT WORK IN REGION =
																		// "EUROPE"
																		// - make the statement work for any region ->
																		// use
																		// prepare statement
		String sql = "select * from employees as e inner join hr.departments as d on e.DEPARTMENT_ID = d.DEPARTMENT_ID "
				+ "inner join hr.locations as l on d.LOCATION_ID = l.LOCATION_ID inner join hr.countries as c on	"
				+ "l.COUNTRY_ID = c.COUNTRY_ID inner join hr.regions as r on c.REGION_ID = r.REGION_ID where REGION_NAME = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, regionName);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String email = resultSet.getString("email");

				System.out.print("EmployeeId : " + employeeId);
				System.out.print(" FirstName: " + firstName);
				System.out.print(" LastName: " + lastName);
				System.out.println(" Email: " + email);
			}
			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getEmployeesInUsaBasedOnSalary() { // SELECT ALL EMPLOYEES WILL SALARIES BETWEEN 10 AND 100
															// WORKING IN THE IT DEPARTMENT IN SPAIN(USA)
		String sql = "select * from employees as e inner join hr.departments as d on e.DEPARTMENT_ID = d.DEPARTMENT_ID "
				+ "inner join hr.locations as l on d.LOCATION_ID = l.LOCATION_ID inner join hr.countries as c on l.COUNTRY_ID = c.COUNTRY_ID "
				+ "where e.SALARY > 1000 and e.SALARY < 1000000 and COUNTRY_NAME = 'United States of America'";

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");
				String email = resultSet.getString("email");

				System.out.print("EmployeeId : " + employeeId);
				System.out.print(" FirstName: " + firstName);
				System.out.print(" LastName: " + lastName);
				System.out.println(" Email: " + email);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getMaxSalaryForJobTitle(String jobTitle) { // SELECT THE MAXIMUM SALARY FOR THE EMPLOYEES THAT
																	// WORK AS SOFTWARE DEVELOPERS-make the
																	// statement work for any job -> use prepare
																	// statement. So instead of software developer I
																	// can send any job_title

		String sql = "select max(salary) from employees as e inner join jobs as j on e.JOB_ID = j.JOB_ID where JOB_TITLE = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, jobTitle);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				double salary = resultSet.getDouble("salary");
				System.out.println("Salary:" + salary);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getEmployeesFromCountryName(String countryName) { // SELECT ALL MANAGERS WHICH WORK IN
																			// CLUJ-NAPOCA - or
		// any city you have in the database
		String sql = "select employee_id,first_name,last_name from employees as e inner join departments as d on e.DEPARTMENT_ID = d.DEPARTMENT_ID "
				+ "inner join locations as l on d.LOCATION_ID = l.LOCATION_ID inner join countries as c on l.COUNTRY_ID = c.COUNTRY_ID "
				+ "where COUNTRY_NAME = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, countryName);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				int employeeId = resultSet.getInt("employee_id");
				String firstName = resultSet.getString("first_name");
				String lastName = resultSet.getString("last_name");

				System.out.println("Employee Id: " + employeeId);
				System.out.println("First Name: " + firstName);
				System.out.println("Last Name: " + lastName);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getCountriesFromLocation() { // SELECT ALL COUNTRIES THAT DON'T HAVE EMPLOYEES.
		String sql = "select * from countries as c inner join locations as l on l.COUNTRY_ID = c.COUNTRY_ID where l.COUNTRY_ID = 'US'";

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String countryId = resultSet.getString("country_id");
				String countryName = resultSet.getString("country_name");

				System.out.println("Country Id: " + countryId);
				System.out.println("Country Name: " + countryName);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getJobForEmployee(String firstName, String lastName) { // SELECT ALL JOBS FOR EMPLOYEE 'Ramona
																				// Cristea' - make the statement generic
																				// so I can send any first and last name
		String sql = "select * from jobs as j inner join employees as e on e.JOB_ID = j.JOB_ID where FIRST_NAME = ? and LAST_NAME = ?";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				String jobId = resultSet.getString("job_id");
				String jobTitle = resultSet.getString("job_title");

				System.out.println("Job Id: " + jobId);
				System.out.println("Job Title: " + jobTitle);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void getMinSalaryForEmployee(String firstName, String lastName) { // SELECT THAT MINIMUM SALARY FOR
																					// EMPLOYEE 'Ramona Cristea' - make
																					// // the statement generic so I can
																					// send any first and last name
		String sql = "select min_salary from jobs as j inner join employees as e "
				+ "on e.JOB_ID = j.JOB_ID where FIRST_NAME = ? and LAST_NAME = ?;";

		try {
			PreparedStatement preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, firstName);
			preparedStatement.setString(2, lastName);
			ResultSet resultSet = preparedStatement.executeQuery();

			while (resultSet.next()) {
				double minSalary = resultSet.getDouble("min_salary");

				System.out.println("Min salary:" + minSalary);
			}

			resultSet.close();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getDepartmentNameWithHighestNumbersOfEmployees() { // SELECT THE DEPARTMENT THAT HAS THE HIGHEST
																			// NUMBER OF EMPLOYEES.
		String sql = "select department_name from departments as d inner join employees as e on e.DEPARTMENT_ID = d.DEPARTMENT_ID "
				+ "group by DEPARTMENT_NAME order by count(*) desc";

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String departmentName = resultSet.getString("department_name");

				System.out.println("Department name: " + departmentName);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getCountryNameWithMaxDepartments() { // SELECT THE COUNTRY THAT HAS THE HIGHEST NUMBER OF
															// DEPARTMENTS
		String sql = "select country_name from countries as c inner join locations as l on l.COUNTRY_ID = c.COUNTRY_ID "
				+ "inner join departments as d on d.LOCATION_ID = l.LOCATION_ID group by COUNTRY_NAME order by count(*) desc";

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				String countryName = resultSet.getString("country_name");

				System.out.println("Country name: " + countryName);
			}

			resultSet.close();
			statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void getAverageSalaryForDepartments() { // SELECT THE AVERAGE SALARY FOR EACH DEPARTMENT
		String sql = "select avg(salary) from employees as e inner join departments as d on d.DEPARTMENT_ID = e.DEPARTMENT_ID "
				+ "group by salary order by avg(SALARY) desc";

		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(sql);

			while (resultSet.next()) {
				double averageSalary = resultSet.getDouble("salary");

				System.out.println("Average salary: " + averageSalary);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
